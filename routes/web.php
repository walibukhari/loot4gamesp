<?php

use Google\AdsApi\AdWords\v201809\cm\CampaignService;
use Illuminate\Support\Facades\Route;
use Edujugon\GoogleAds\GoogleAds;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/data',function () {
    $ads = new GoogleAds();
    $ads->env('test')
        ->oAuth([
            'clientId' => 'test',
            'clientSecret' => 'test',
            'refreshToken' => 'test'

        ])
        ->session([
            'developerToken' => 'token',
            'clientCustomerId' => 'id'
        ]);
    $ads->service(CampaignService::class);
    $ads->service(CampaignService::class)
        ->select(['Id', 'Name', 'Status', 'ServingStatus', 'StartDate', 'EndDate'])
        ->get();
    dd($ads);
});

Route::get('auth/google/callback', function() {
    return redirect('/data');
});

Route::get('/{any}', 'ApplicationController')->where('any', '.*');
