<?php
return [
    //Environment=> test/production
    'env' => 'test',
    //Google Ads
    'production' => [
        'developerToken' => env("YOUR_DEV_TOKEN"),
        'clientCustomerId' => env("CLIENT_CUSTOMER_ID"),
        'userAgent' => env("YOUR_NAME"),
        'clientId' => env("CLIENT_ID"),
        'clientSecret' => env("CLIENT_SECRET"),
        'refreshToken' => env("REFRESH_TOKEN")
    ],
    'test' => [
        'developerToken' => env("YOUR_DEV_TOKEN"),
        'clientCustomerId' => env("CLIENT_CUSTOMER_ID"),
        'userAgent' => env("YOUR_NAME"),
        'clientId' => env("CLIENT_ID"),
        'clientSecret' => env("CLIENT_SECRET"),
        'refreshToken' => env("REFRESH_TOKEN")
    ],
    'oAuth2' => [
        'authorizationUri' => 'https://accounts.google.com/o/oauth2/v2/auth',
        'redirectUri' => 'https://localhost:8000/auth/google/callback',
        'tokenCredentialUri' => 'https://www.googleapis.com/oauth2/v4/token',
        'scope' => 'https://www.googleapis.com/auth/adwords'
    ]
];
